export class Product {
    productId: number;
    productName: string;
    price: number;
    pictureURL: string;
    authorOrDesign: string;
    genreOrType: string;
    publicationOrBrand: string;


    productAddedInCart:boolean;
    

    constructor(id: number, name: string, price: number, productAddedInCart:boolean, pictureURL:string, AuthorOrDesign: string, GenreOrType:string, PublicationOrBrand:string  ) {
        this.productId = id;
        this.productName = name;
        this.price = price;
        this.productAddedInCart = productAddedInCart;
        this.pictureURL=pictureURL;
        this.authorOrDesign=AuthorOrDesign;
        this.genreOrType=GenreOrType;
        this.publicationOrBrand=PublicationOrBrand;
        
    }
}
