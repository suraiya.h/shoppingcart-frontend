import { Product } from './product.model';
import { productDtos } from './cart-item.model';

export class Cart {
    
    totalQuantity: number;
    totalAmount: number;
    products: productDtos[];
    

    constructor(products: productDtos[], totalQuantity: number, totalAmount: number ) {
      
        this.totalQuantity = totalQuantity;
        this.totalAmount = totalAmount;
        this.products = products;


        
        
    }
}