import { Component, OnInit } from '@angular/core';
import { ShoppingCartService } from '../services/shopping-cart.service';
import { Cart } from '../models/cart.model';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faMinus } from '@fortawesome/free-solid-svg-icons';
import { ToastrService } from 'ngx-toastr';
import { Product } from '../models/product.model';
import { productDtos } from '../models/cart-item.model';


@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  // cartitems: Cart[];
  cart : Cart;
  faPlus=faPlus;
  faMinus=faMinus;
  totalQuantity:number;
  totalAmount: number;
  productitems :productDtos[];
  quantity:[];

  constructor(private shoppingCartService: ShoppingCartService, private toasterService: ToastrService) { }

  ngOnInit(): void {
    this.loadCart();
  }

  

 loadCart(){
    this.shoppingCartService.getAllCartItems()
        .subscribe(


          (cart: Cart) => {
            this.cart=cart;
            this.totalAmount=cart.totalAmount;
            this.totalQuantity=cart.totalQuantity;
            this.productitems= cart["productDtos"];
          },         
          
      );
     

  }

  removeFromCart(productId){
    this.shoppingCartService.removeFromCart(productId).subscribe(()=>{
      this.loadCart();
    });
     
       
  
  }




removeAllFromCart(){
  this.shoppingCartService.removeAllFromCart().subscribe(()=>{
    this.loadCart();
}
  );
}

checkout(){
  this.toasterService.success("Ordered!", "Success", { positionClass: 'toast-bottom-center' });

}

increaseQuantity(productId){
  console.log(productId);
  this.shoppingCartService.increaseQuantity(productId).subscribe(()=>{
    this.loadCart();
}
  );

}

decreaseQuantity(productId){
  console.log(productId);
  this.shoppingCartService.decreaseQuantity(productId).subscribe(()=>{
    this.loadCart();
}
  );
  

}

}

