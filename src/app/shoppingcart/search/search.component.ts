import { Component, OnInit } from '@angular/core';
import { ProductCart } from '../models/product-cart.model';
import { Product } from '../models/product.model';
import { ShoppingCartService } from '../services/shopping-cart.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  products: Product[] = [];
  productCarts: ProductCart[] = [];
  search:string;
  

  constructor(private shoppingCartService : ShoppingCartService) { }

  ngOnInit(): void {
  }

  searchQuery() {
    this.products = [];
    this.productCarts = [];
    this.shoppingCartService.searchProducts(this.search)
        .subscribe(
            (products: any[]) => {
                this.products = products;
                this.products.forEach(product => {
                    this.productCarts.push(new ProductCart(product, 0));
                })
            },
            (error) => alert(error)
        );
}

addToCart(productId,quantity) {
  console.log(productId,quantity);
  this.shoppingCartService.addToCart(productId,quantity).subscribe(()=>{
    
    this.searchQuery();
  });      
}



removeFromCart(productId){
  this.shoppingCartService.removeFromCart(productId).subscribe(()=>{
    this.searchQuery();
  });
   
     

}

}
