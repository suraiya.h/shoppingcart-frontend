import { Injectable } from '@angular/core';
import {HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import { Product } from '../models/product.model';
import { ProductCart } from '../models/product-cart.model';
import { Observable } from 'rxjs';
import { Cart } from '../models/cart.model';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {
  private productsUrl = "/api/viewAllProducts/101";
  private userId="101";


  constructor(private http: HttpClient) { }


  getAllProducts() {
    return this.http.get(this.productsUrl);
}

searchProducts(search){
  const url = `/api/searchProduct/${search}`;
  return this.http.get(url);

}


addToCart(productId,quantity){
  const url = `/api/addProductInCart/${productId}/${this.userId}/${quantity}`;
  return this.http.post(url, productId );

}

removeFromCart(productId){
  console.log(productId)
  const url = `/api/removeProductById/${productId}/${this.userId}`;
  return this.http.delete(url, productId );

}

getAllCartItems(){

  const url = `/api/viewCartById/${this.userId}`;
  console.log(url);
  return this.http.get(url);

}

removeAllFromCart(){
  
  const url = `/api/removeAllProduct/101`;
  console.log(url);
  return this.http.delete(url);

}

increaseQuantity(productId){
  const url = `/api/increaseQuantity/${this.userId}/${productId}`;
  console.log(url);
  return this.http.put(url, productId);

}

decreaseQuantity(productId){
  const url = `/api/decreaseQuantity/${this.userId}/${productId}`;
  console.log(url);
  return this.http.put(url,productId);

}



}
