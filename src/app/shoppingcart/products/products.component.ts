import { Component, OnInit } from '@angular/core';
import { ShoppingCartService } from '../services/shopping-cart.service'
import { Product } from '../models/product.model';
import { ProductCart } from '../models/product-cart.model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  products: Product[] = [];
  productCarts: ProductCart[] = [];

  constructor(private shoppingCartService : ShoppingCartService) { }

  ngOnInit(): void {
    this.loadProducts();
  }

  loadProducts() {
    this.products = [];
    this.productCarts = [];
    this.shoppingCartService.getAllProducts()
        .subscribe(
            (products: any[]) => {
                this.products = products;
                console.log(products);
                this.products.forEach(product => {
                    this.productCarts.push(new ProductCart(product, 0));
                })
            },
            (error) => console.log(error)
        );
}

addToCart(productId,quantity) {
  console.log(productId,quantity);
  this.shoppingCartService.addToCart(productId,quantity).subscribe(()=>{
    
    this.loadProducts();
  });
  
   
      
       
}

removeFromCart(productId){
  this.shoppingCartService.removeFromCart(productId).subscribe(()=>{
    this.loadProducts();
  });
   
     

}



}
