import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductsComponent } from './shoppingcart/products/products.component';
import { CartComponent } from './shoppingcart/cart/cart.component';
import { SearchComponent } from './shoppingcart/search/search.component';


const routes: Routes = [
  { path: '', component: ProductsComponent },
  { path: 'mycart', component: CartComponent },
  { path: 'search', component: SearchComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
